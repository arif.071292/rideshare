from rest_framework import serializers
from django.contrib.auth.models import User
from models import Admin, Passenger, Company, Driver, Taxi


class AdminSerializer(serializers.ModelSerializer):
    class Meta:
        model      	= Admin
        fields      	= ('id', 'name', 'role', 'email', 'propic', 'info', 'created', 'updated')
        read_only_fields = ('role', 'email', 'created')


class CreateAdminSerializer(serializers.ModelSerializer):
    class Meta:
    	model = Admin
    	fields = ('id', 'email', 'role', 'password', 'created')


class PassengerSerializer(serializers.ModelSerializer):
    class Meta:
        model 	= Passenger
        fields 	= ('id', 'name', 'role', 'email', 'university', 'community', 'propic', 'info', 'created', 'updated')
        read_only_fields = ('role', 'email', 'created')


class CreatePassengerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Passenger
        fields = ('id', 'name', 'email', 'role', 'password', 'university', 'community', 'created')


class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ('id', 'name', 'role', 'email', 'license', 'propic', 'info', 'created', 'updated')
        read_only_fields = ('role', 'email', 'created')


class CreateCompanySerializer(serializers.ModelSerializer):
    class Meta:
    	model = Company
    	fields = ('id', 'email', 'role', 'password', 'created')


class DriverSerializer(serializers.ModelSerializer):
    class Meta:
        model = Driver
        fields = ('id', 'company', 'name', 'role', 'email', 'license', 'propic', 'info', 'created', 'updated')
        read_only_fields = ('role', 'email', 'created')


class CreateDriverSerializer(serializers.ModelSerializer):
    class Meta:
    	model = Driver
    	fields = ('id', 'email', 'role', 'password', 'created')


class TaxiSerializer(serializers.ModelSerializer):
    class Meta:
        model = Taxi
        fields = ('id', 'company', 'driver', 'license','created', 'updated')
        read_only_fields = ('company', 'created')


class LoginSerializer(serializers.Serializer):
    email    = serializers.CharField()
    password    = serializers.CharField()


class PasswordSerializer(serializers.Serializer):
    email      = serializers.EmailField()
    old_password    = serializers.CharField()
    new_password    = serializers.CharField()