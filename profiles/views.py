from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User
from locations.models import University
from models import Admin, Passenger, Company, Driver, Taxi
from serializers import AdminSerializer, CreateAdminSerializer
from serializers import PassengerSerializer, CompanySerializer, DriverSerializer, TaxiSerializer
from serializers import CreatePassengerSerializer, CreateCompanySerializer, CreateDriverSerializer
from serializers import LoginSerializer, PasswordSerializer


@api_view(['POST'])
def create_admin(request):
    """
    create a new admin.
    ---
    request_serializer: CreateAdminSerializer
    """
    if request.method == 'POST':
        response = {}
        serializer = CreateAdminSerializer(data=request.data)

        if serializer.is_valid():
            email   = serializer.validated_data['email']
            passwd  = serializer.validated_data['password']
            role = serializer.validated_data['role']
            serializer.validated_data['password'] = make_password(passwd)

            if role == 'admin':
                if len(passwd) <5:
                    response['password'] = "password length must be at least 5"
                    return Response(response, status=status.HTTP_400_BAD_REQUEST)

                if " " in passwd:
                    response['password'] = "no whitespace allowed in password"
                    return Response(response, status=status.HTTP_400_BAD_REQUEST)

                try:
                    user = User(username=email, email=email, password=make_password(passwd), first_name=role)
                    user.save()

                    try:
                        serializer.save()
                    except :
                        user.delete()
                        return Response(status=status.HTTP_400_BAD_REQUEST)

                    return Response(status=status.HTTP_201_CREATED)
                except:
                    return Response(status=status.HTTP_400_BAD_REQUEST)
            else:
                response['role'] = "Not Admin!"
                return Response(response, status=status.HTTP_400_BAD_REQUEST)
            
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def admin_list(request, pageName, pageSize):
    """
    list all admins
    """
    page,size = int(pageName), int(pageSize)
    start = (page-1)*size
    end = start+size

    if request.method == 'GET':
        admins = Admin.objects.all()[start:end]
        serializer = AdminSerializer(admins, many=True)
        return Response(serializer.data)


@api_view(['GET', 'PUT', 'DELETE'])
def admin_detail(request, id):
    """
    retrieve, update or delete a admin instance.
    ---
    request_serializer: AdminSerializer
    """
    
    try:
        admin = Admin.objects.get(pk=id)
    except Admin.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = AdminSerializer(admin)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = AdminSerializer(admin, partial=True, data=request.data)
        if serializer.is_valid():
            serializer.save()
            serializer = AdminSerializer(admin)
            return Response(status=status.HTTP_204_NO_CONTENT)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        try:
            user = User.objects.get(email=admin.email)
            user.delete()
            admin.delete()
        except Admin.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['POST'])
def create_passenger(request):
    """
    create a new passenger.
    ---
    request_serializer: CreatePassengerSerializer
    """

    if request.method == 'POST':
        response = {}
        serializer = CreatePassengerSerializer(data=request.data)

        if serializer.is_valid():
            email   = serializer.validated_data['email']
            passwd  = serializer.validated_data['password']
            role = serializer.validated_data['role']
            serializer.validated_data['password'] = make_password(passwd)
            university = serializer.validated_data['university'].id
            print university

            if role == 'passenger':
                uni = University.objects.get(id=int(university))
                if email[-len(uni.email_ext):] != uni.email_ext:
                    response['password'] = "Invalid Email Extension!"
                    return Response(response, status=status.HTTP_400_BAD_REQUEST)

                if len(passwd) <5:
                    response['password'] = "password length must be at least 5"
                    return Response(response, status=status.HTTP_400_BAD_REQUEST)

                if " " in passwd:
                    response['password'] = "no whitespace allowed in password"
                    return Response(response, status=status.HTTP_400_BAD_REQUEST)

                try:
                    user = User(username=email, email=email, password=make_password(passwd), first_name=role)
                    user.save()

                    try:
                        serializer.save()
                    except :
                        user.delete()
                        return Response(status=status.HTTP_400_BAD_REQUEST)

                    return Response(status=status.HTTP_201_CREATED)
                except:
                    return Response(status=status.HTTP_400_BAD_REQUEST)
            else:
                response['role'] = "Not Passenger!"
                return Response(response, status=status.HTTP_400_BAD_REQUEST)
            
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def passenger_list(request, pageName, pageSize):
    """
    list all passengers
    """
    page,size = int(pageName), int(pageSize)
    start = (page-1)*size
    end = start+size

    if request.method == 'GET':
        passengers = Passenger.objects.all()[start:end]
        serializer = PassengerSerializer(passengers, many=True)
        return Response(serializer.data)


@api_view(['GET', 'PUT', 'DELETE'])
def passenger_detail(request, id):
    """
    retrieve, update or delete a passenger instance.
    ---
    request_serializer: PassengerSerializer
    """
    
    try:
        passenger = Passenger.objects.get(pk=id)
    except Passenger.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = PassengerSerializer(passenger)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = PassengerSerializer(passenger, partial=True, data=request.data)
        if serializer.is_valid():
            serializer.save()
            serializer = PassengerSerializer(passenger)
            return Response(status=status.HTTP_204_NO_CONTENT)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        try:
            user = User.objects.get(email=passenger.email)
            user.delete()
            passenger.delete()
        except Passenger.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['POST'])
def create_company(request):
    """
    create a new company.
    ---
    request_serializer: CreateCompanySerializer
    """\

    if request.method == 'POST':
        response = {}
        serializer = CreateCompanySerializer(data=request.data)

        if serializer.is_valid():
            email   = serializer.validated_data['email']
            passwd  = serializer.validated_data['password']
            role = serializer.validated_data['role']
            serializer.validated_data['password'] = make_password(passwd)

            if role == 'company':
                if len(passwd) <5:
                    response['password'] = "password length must be at least 5"
                    return Response(response, status=status.HTTP_400_BAD_REQUEST)

                if " " in passwd:
                    response['password'] = "no whitespace allowed in password"
                    return Response(response, status=status.HTTP_400_BAD_REQUEST)

                try:
                    user = User(username=email, email=email, password=make_password(passwd), first_name=role)
                    user.save()

                    try:
                        serializer.save()
                    except :
                        user.delete()
                        return Response(status=status.HTTP_400_BAD_REQUEST)

                    return Response(status=status.HTTP_201_CREATED)
                except:
                    return Response(status=status.HTTP_400_BAD_REQUEST)
            else:
                response['role'] = "Not Company!"
                return Response(response, status=status.HTTP_400_BAD_REQUEST)
            
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def company_list(request, pageName, pageSize):
    """
    list all companys
    """
    page,size = int(pageName), int(pageSize)
    start = (page-1)*size
    end = start+size

    if request.method == 'GET':
        companys = Company.objects.all()[start:end]
        serializer = CompanySerializer(companys, many=True)
        return Response(serializer.data)


@api_view(['GET', 'PUT', 'DELETE'])
def company_detail(request, id):
    """
    retrieve, update or delete a company instance.
    ---
    request_serializer: CompanySerializer
    """
    
    try:
        company = Company.objects.get(pk=id)
    except Company.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = CompanySerializer(company)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = CompanySerializer(company, partial=True, data=request.data)
        if serializer.is_valid():
            serializer.save()
            serializer = CompanySerializer(company)
            return Response(status=status.HTTP_204_NO_CONTENT)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        try:
            user = User.objects.get(email=company.email)
            user.delete()
            company.delete()
        except Company.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['POST'])
def create_driver(request):
    """
    create a new driver.
    ---
    request_serializer: CreateDriverSerializer
    """

    if request.method == 'POST':
        response = {}
        serializer = CreateDriverSerializer(data=request.data)

        if serializer.is_valid():
            email   = serializer.validated_data['email']
            passwd  = serializer.validated_data['password']
            role = serializer.validated_data['role']
            serializer.validated_data['password'] = make_password(passwd)

            if role == 'driver':
                if len(passwd) <5:
                    response['password'] = "password length must be at least 5"
                    return Response(response, status=status.HTTP_400_BAD_REQUEST)

                if " " in passwd:
                    response['password'] = "no whitespace allowed in password"
                    return Response(response, status=status.HTTP_400_BAD_REQUEST)

                try:
                    user = User(username=email, email=email, password=make_password(passwd), first_name=role)
                    user.save()

                    try:
                        serializer.save()
                    except :
                        user.delete()
                        return Response(status=status.HTTP_400_BAD_REQUEST)

                    return Response(status=status.HTTP_201_CREATED)
                except:
                    return Response(status=status.HTTP_400_BAD_REQUEST)
            else:
                response['role'] = "Not Admin!"
                return Response(response, status=status.HTTP_400_BAD_REQUEST)
            
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def driver_list(request, pageName, pageSize):
    """
    list all drivers
    """
    page,size = int(pageName), int(pageSize)
    start = (page-1)*size
    end = start+size

    if request.method == 'GET':
        drivers = Driver.objects.all()[start:end]
        serializer = DriverSerializer(drivers, many=True)
        return Response(serializer.data)


@api_view(['GET', 'PUT', 'DELETE'])
def driver_detail(request, id):
    """
    retrieve, update or delete a driver instance.
    ---
    request_serializer: DriverSerializer
    """
    
    try:
        driver = Driver.objects.get(pk=id)
    except Driver.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = DriverSerializer(driver)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = DriverSerializer(driver, partial=True, data=request.data)
        if serializer.is_valid():
            serializer.save()
            serializer = DriverSerializer(driver)
            return Response(status=status.HTTP_204_NO_CONTENT)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        try:
            user = User.objects.get(email=driver.email)
            user.delete()
            driver.delete()
        except Driver.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'POST'])
def taxi_list(request):
    """
    List all taxies, or create a new taxi.
    ---
    request_serializer: TaxiSerializer
    """
    if request.method == 'GET':
        taxies = Taxi.objects.all()
        serializer = TaxiSerializer(taxies, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = TaxiSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
def taxi_detail(request, id):
    """
    retrieve, update or delete a taxi instance.
    ---
    request_serializer: TaxiSerializer
    """

    try:
        taxi = Taxi.objects.get(pk=id)
    except Taxi.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = TaxiSerializer(taxi)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = TaxiSerializer(taxi, partial=True, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        taxi.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['POST'])
def login(request):
    """
    login user.
    ---
    request_serializer: LoginSerializer
    """

    if request.method == 'POST':
        if request.data['email']:
            email   = request.data['email']
        else:
            email   = ""

        if request.data["password"]:
            passwd  = request.data['password']
        else:
            passwd  = ""

        try:
            user = User.objects.get(email=email)
        except User.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        if(user.check_password(passwd)):
            if user.first_name == "admin":
                admin = Admin.objects.get(email=user.email)
                serializer = AdminSerializer(admin)

            if user.first_name == "passenger":
                passenger = Passenger.objects.get(email=user.email)
                serializer = PassengerSerializer(passenger)

            if user.first_name == "company":
                company = Company.objects.get(email=user.email)
                serializer = CompanySerializer(company)

            if user.first_name == "driver":
                driver = Driver.objects.get(email=user.email)
                serializer = DriverSerializer(driver)

            return Response(serializer.data)


        return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view(['PUT'])
def change_password(request):
    """
    update password of a user instance.
    ---
    request_serializer: PasswordSerializer
    """
    data = request.data
    email = data['email']
    passwd1 = data['old_password']
    passwd2 = data['new_password']

    try:
        user = User.objects.get(email=email)
    except User.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if not user.check_password(passwd1):
        response = {}
        response['password'] = "password doesn't match. enter correct password."
        return Response(response, status=status.HTTP_400_BAD_REQUEST)

    if len(passwd2) <5:
        response = {}
        response['password'] = "password length must be at least 5"
        return Response(response, status=status.HTTP_400_BAD_REQUEST)

    if " " in passwd2:
        response = {}
        response['password'] = "no whitespace allowed in password"
        return Response(response, status=status.HTTP_400_BAD_REQUEST)

    user.set_password(passwd2)
    try:
        user.save()
        return Response(status=status.HTTP_204_NO_CONTENT)
    except:
        return Response(status=status.HTTP_400_BAD_REQUEST)