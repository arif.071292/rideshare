from django.conf.urls import url, include
from rest_framework import routers
from django.contrib import admin

router = routers.DefaultRouter()

urlpatterns = [
	url(r'^', include(router.urls)),
	url(r'^', include('rides.urls')),
	url(r'^', include('locations.urls')),
	url(r'^', include('profiles.urls')),
	url(r'^', include('notifications.urls')),
    url(r'^docs/', include('rest_framework_swagger.urls')),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]