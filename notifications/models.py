from django.db import models
from django.contrib.auth.models import User

class Notification(models.Model):
    user     	=  models.ForeignKey(User, on_delete=models.CASCADE)
    content     = models.CharField(max_length=300)
    created     = models.DateTimeField(auto_now_add=True, auto_now=False)

    class meta:
        ordering = ['user', '-created']
