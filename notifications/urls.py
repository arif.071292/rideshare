from django.conf.urls import url
from notifications import views

urlpatterns = [
	url(r'^notification/$', views.create_notification),
    url(r'^notification/user/(?P<id>\d+)/page/(?P<pageName>\d+)/size/(?P<pageSize>\d+)/$', views.user_notification_list),
    url(r'^notification/(?P<id>[0-9]+)/$', views.notification_detail),
]