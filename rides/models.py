from django.db import models
from django.contrib.auth.models import User
from locations.models import Location
from profiles.models import Passenger, Company, Driver


class Ride(models.Model):
    creator             = models.ForeignKey(Passenger, related_name='creator', on_delete=models.CASCADE)
    source              = models.ForeignKey(Location, related_name="source", on_delete=models.CASCADE)
    destination         = models.ForeignKey(Location, related_name="destination", on_delete=models.CASCADE,db_index=True)
    passenger_required  = models.IntegerField()
    passenger_onboard   = models.IntegerField(default=1)
    driver              = models.ForeignKey(Driver, related_name='driverid', on_delete=models.CASCADE, null=True)
    start_time          = models.DateTimeField()
    completed           = models.DateTimeField(null=True)
    active              = models.BooleanField(default=True)
    created             = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated             = models.DateTimeField(auto_now_add=False, auto_now=True)

    class Meta:
        ordering = ['-created']


class Invitation(models.Model):
    sender      = models.ForeignKey(Passenger, related_name='sender', on_delete=models.CASCADE)
    receiver    = models.ForeignKey(Passenger, related_name='receiver', on_delete=models.CASCADE)
    ride        = models.ForeignKey(Ride, on_delete=models.CASCADE,db_index=True)
    created     = models.DateTimeField(auto_now_add=True, auto_now=False)

    class Meta:
        ordering        = ['ride', '-created']
        unique_together = ('receiver', 'ride')


class RideLog(models.Model):
    ride        = models.ForeignKey(Ride, on_delete=models.CASCADE,db_index=True)
    passenger   = models.ForeignKey(Passenger, on_delete=models.CASCADE)
    created     = models.DateTimeField(auto_now_add=True, auto_now=False)

    class Meta:
        ordering        = ['ride', '-created']
        unique_together = ('ride', 'passenger')


class RideApplyPassenger(models.Model):
    ride        = models.ForeignKey(Ride, on_delete=models.CASCADE,db_index=True)
    passenger   = models.ForeignKey(Passenger, on_delete=models.CASCADE)
    created     = models.DateTimeField(auto_now_add=True, auto_now=False)

    class Meta:
        ordering        = ['ride', '-created']
        unique_together = ('ride', 'passenger')


class RideApplyDriver(models.Model):
    ride    = models.ForeignKey(Ride, on_delete=models.CASCADE, db_index=True)
    driver  = models.ForeignKey(Driver, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)

    class Meta:
        ordering        = ['ride', '-created']
        unique_together = ('ride', 'driver')
