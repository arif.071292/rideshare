import datetime
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User
from models import Ride, Invitation, RideLog, RideApplyPassenger, RideApplyDriver
from serializers import RideSerializer, CreateRideSerializer, InvitationSerializer
from serializers import RideLogSerializer, RideApplyPassengerSerializer, RideApplyDriverSerializer


@api_view(['POST'])
def create_ride(request):
    """
    create a new ride.
    ---
    request_serializer: CreateRideSerializer
    """

    if request.method == 'POST':
        serializer = CreateRideSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def ride_list(request, pageName, pageSize):
    """
    list all rides
    """
    page,size = int(pageName), int(pageSize)
    start = (page-1)*size
    end = start+size

    if request.method == 'GET':
        rides = Ride.objects.all()[start:end]
        serializer = RideSerializer(rides, many=True)
        return Response(serializer.data)


@api_view(['GET'])
def ride_creator(request, id, pageName, pageSize):
    """
    list all rides of a creator
    """
    page,size = int(pageName), int(pageSize)
    start = (page-1)*size
    end = start+size

    if request.method == 'GET':
        rides = Ride.objects.filter(creator=id)[start:end]
        serializer = RideSerializer(rides, many=True)
        return Response(serializer.data)


@api_view(['GET'])
def ride_destination(request, id, pageName, pageSize):
    """
    list all rides of a destinaton
    """
    page,size = int(pageName), int(pageSize)
    start = (page-1)*size
    end = start+size

    if request.method == 'GET':
        now = datetime.datetime.now()
        rides = Ride.objects.filter(destination=id,start_time_gt=now)[start:end]
        serializer = RideSerializer(rides, many=True)
        return Response(serializer.data)


@api_view(['GET'])
def ride_source_destination(request, id1, id2, pageName, pageSize):
    """
    list all rides of a creator
    """
    page,size = int(pageName), int(pageSize)
    start = (page-1)*size
    end = start+size

    if request.method == 'GET':
        now = datetime.datetime.now()
        rides = Ride.objects.all(source=id1, destination=id2,  start_time_gt=now)[start:end]
        serializer = RideSerializer(rides, many=True)
        return Response(serializer.data)


@api_view(['GET', 'PUT', 'DELETE'])
def ride_detail(request, id):
    """
    retrieve or delete a ride instance.
    ---
    request_serializer : RideSerializer
    """

    try:
        ride = Ride.objects.get(pk=id)
    except Ride.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = RideSerializer(ride)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = RideSerializer(ride, partial=True, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        ride.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'POST'])
def ride_log_list(request):
    """
    List all ride_logs, or create a new ride_log.
    ---
    request_serializer: RideLogSerializer
    """
    if request.method == 'GET':
        ride_logs = RideLog.objects.all()
        serializer = RideLogSerializer(ride_logs, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = RideLogSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def ride_log_passenger(request, id, pageName, pageSize):
    """
    list all rides of a passenger
    """
    page,size = int(pageName), int(pageSize)
    start = (page-1)*size
    end = start+size

    if request.method == 'GET':
        rides = RideLog.objects.filter(passenger=id)[start:end]
        serializer = RideLogSerializer(rides, many=True)
        return Response(serializer.data)


@api_view(['GET'])
def ride_log_ride(request, id, pageName, pageSize):
    """
    list all passengers of a ride
    """
    page,size = int(pageName), int(pageSize)
    start = (page-1)*size
    end = start+size

    if request.method == 'GET':
        rides = RideLog.objects.filter(ride=id)[start:end]
        serializer = RideLogSerializer(rides, many=True)
        return Response(serializer.data)


@api_view(['GET', 'PUT', 'DELETE'])
def ride_log_detail(request, id):
    """
    retrieve, update or delete a ride_log instance.
    ---
    request_serializer: RideLogSerializer
    """

    try:
        ride_log = RideLog.objects.get(pk=id)
    except RideLog.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = RideLogSerializer(ride_log)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = RideLogSerializer(ride_log, partial=True, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        ride_log.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'POST'])
def ride_passenger_list(request):
    """
    List all passengers application, or create a new ride application.
    ---
    request_serializer: RideApplyPassengerSerializer
    """
    if request.method == 'GET':
        passengers = RideApplyPassenger.objects.all()
        serializer = RideApplyPassengerSerializer(passengers, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = RideApplyPassengerSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def ride_apply_passenger(request, id, pageName, pageSize):
    """
    list all ride applications of a passenger
    """
    page,size = int(pageName), int(pageSize)
    start = (page-1)*size
    end = start+size

    if request.method == 'GET':
        rides = RideApplyPassenger.objects.filter(passenger=id)[start:end]
        serializer = RideApplyPassengerSerializer(rides, many=True)
        return Response(serializer.data)


@api_view(['GET'])
def ride_apply_passenger_ride(request, id, pageName, pageSize):
    """
    list all applied passengers of a ride
    """
    page,size = int(pageName), int(pageSize)
    start = (page-1)*size
    end = start+size

    if request.method == 'GET':
        rides = RideApplyPassenger.objects.filter(ride=id)[start:end]
        serializer = RideApplyPassengerSerializer(rides, many=True)
        return Response(serializer.data)


@api_view(['GET', 'PUT', 'DELETE'])
def ride_passenger_detail(request, id):
    """
    retrieve, update or delete a ride_passenger instance.
    ---
    request_serializer: RideApplyPassengerSerializer
    """

    try:
        ride_passenger = RideApplyPassenger.objects.get(pk=id)
    except RideApplyPassenger.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = RideApplyPassengerSerializer(ride_passenger)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = RideApplyPassengerSerializer(ride_passenger, partial=True, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        ride_passenger.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'POST'])
def ride_driver_list(request):
    """
    List all drivers, or create a new ride_driver.
    ---
    request_serializer: RideApplyDriverSerializer
    """
    if request.method == 'GET':
        drivers = RideApplyDriver.objects.all()
        serializer = RideApplyDriverSerializer(drivers, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = RideApplyDriverSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def ride_apply_driver(request, id, pageName, pageSize):
    """
    list all ride applications of a driver
    """
    page,size = int(pageName), int(pageSize)
    start = (page-1)*size
    end = start+size

    if request.method == 'GET':
        rides = RideApplyDriver.objects.filter(driver=id)[start:end]
        serializer = RideApplyDriverSerializer(rides, many=True)
        return Response(serializer.data)


@api_view(['GET'])
def ride_apply_driver_ride(request, id, pageName, pageSize):
    """
    list all applied drivers of a ride
    """
    page,size = int(pageName), int(pageSize)
    start = (page-1)*size
    end = start+size

    if request.method == 'GET':
        rides = RideApplyDriver.objects.filter(ride=id)[start:end]
        serializer = RideApplyDriverSerializer(rides, many=True)
        return Response(serializer.data)

@api_view(['GET', 'PUT', 'DELETE'])
def ride_driver_detail(request, id):
    """
    retrieve, update or delete a ride_driver instance.
    ---
    request_serializer: RideApplyDriverSerializer
    """

    try:
        ride_driver = RideApplyDriver.objects.get(pk=id)
    except RideApplyDriver.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = RideApplyDriverSerializer(ride_driver)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = RideApplyDriverSerializer(ride_driver, partial=True, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        ride_driver.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'POST'])
def invitation_list(request):
    """
    List all invitations, or create a new invitation.
    ---
    request_serializer: InvitationSerializer
    """
    if request.method == 'GET':
        invitations = Invitation.objects.all()
        serializer = InvitationSerializer(invitations, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = InvitationSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def ride_invitation_ride(request, id, pageName, pageSize):
    """
    list all invitations of a ride
    """
    page,size = int(pageName), int(pageSize)
    start = (page-1)*size
    end = start+size

    if request.method == 'GET':
        rides = Invitation.objects.filter(ride=id)[start:end]
        serializer = InvitationSerializer(rides, many=True)
        return Response(serializer.data)


@api_view(['GET', 'PUT', 'DELETE'])
def invitation_detail(request, id):
    """
    retrieve, update or delete a invitation instance.
    ---
    request_serializer: InvitationSerializer
    """

    try:
        invitation = Invitation.objects.get(pk=id)
    except Invitation.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = InvitationSerializer(invitation)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = InvitationSerializer(invitation, partial=True, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        invitation.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)