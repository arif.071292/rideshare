from rest_framework import serializers
from django.contrib.auth.models import User
from models import Ride, Invitation, RideLog, RideApplyPassenger, RideApplyDriver


class RideSerializer(serializers.ModelSerializer):
    class Meta:
        model      	= Ride
        fields      	= ('id', 'creator', 'source', 'destination', 'passenger_required', 'passenger_onboard', 'driver', 'start_time', 'completed', 'active', 'created', 'updated')


class CreateRideSerializer(serializers.ModelSerializer):
    class Meta:
        model      	= Ride
        fields      	= ('id', 'creator', 'source', 'destination', 'passenger_required', 'start_time', 'active', 'created')


class RideLogSerializer(serializers.ModelSerializer):
    class Meta:
        model      	= RideLog
        fields      	= ('id', 'ride', 'passenger', 'created')


class RideApplyPassengerSerializer(serializers.ModelSerializer):
    class Meta:
        model      	= RideApplyPassenger
        fields      	= ('id', 'ride', 'passenger', 'created')


class RideApplyDriverSerializer(serializers.ModelSerializer):
    class Meta:
        model      	= RideApplyDriver
        fields      	= ('id', 'ride', 'driver', 'created')


class InvitationSerializer(serializers.ModelSerializer):
    class Meta:
        model      	= Invitation
        fields      	= ('id', 'sender', 'receiver', 'ride', 'created')


