from django.conf.urls import url
from locations import views

urlpatterns = [
    url(r'^location/$', views.location_list),
    url(r'^location/(?P<id>[0-9]+)/$', views.location_detail),
    url(r'^university/$', views.university_list),
    url(r'^university/(?P<id>[0-9]+)/$', views.university_detail),
    url(r'^community/$', views.create_community),
    url(r'^community/university/(?P<id>\d+)/$', views.university_community_list),
    url(r'^community/(?P<id>[0-9]+)/$', views.community_detail),
]