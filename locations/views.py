from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from models import Location, University, Community
from serializers import LocationSerializer, UniversitySerializer, UpdateUniversitySerializer
from serializers import CommunitySerializer, UpdateCommunitySerializer


###### Location ######

@api_view(['GET', 'POST'])
def location_list(request):
    """
    List all categories, or create a new location.
    ---
    request_serializer: LocationSerializer
    """
    if request.method == 'GET':
        categories = Location.objects.all()
        serializer = LocationSerializer(categories, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = LocationSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



@api_view(['GET', 'PUT', 'DELETE'])
def location_detail(request, id):
    """
    retrieve, update or delete a location instance.
    ---
    request_serializer: LocationSerializer
    """

    try:
        location = Location.objects.get(pk=id)
    except Location.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = LocationSerializer(location)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = LocationSerializer(location, partial=True, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        location.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)



###### University ######

@api_view(['GET', 'POST'])
def university_list(request):
    """
    List all subcategories, or create a new university.
    ---
    request_serializer: UniversitySerializer
    """

    if request.method == 'GET':
        subcategories = University.objects.all()
        serializer = UniversitySerializer(subcategories, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = UniversitySerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
def university_detail(request, id):
    """
    retrieve, update or delete a university instance.
    ---
    request_serializer : UpdateUniversitySerializer
    """

    try:
        university = University.objects.get(pk=id)
    except University.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = UniversitySerializer(university)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = UniversitySerializer(university, partial=True, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        university.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)



###### Community ######

@api_view(['POST'])
def create_community(request):
    """
    create a new community.
    ---
    request_serializer: CommunitySerializer
    """

    if request.method == 'POST':
        serializer = CommunitySerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def university_community_list(request, id):
    """
    list all communitys of given university
    """

    if request.method == 'GET':
        communitys = Community.objects.filter(university=id)
        serializer = CommunitySerializer(communitys, many=True)
        return Response(serializer.data)


@api_view(['GET', 'PUT', 'DELETE'])
def community_detail(request, id):
    """
    retrieve or delete a community instance.
    ---
    request_serializer : UpdateCommunitySerializer
    """

    try:
        community = Community.objects.get(pk=id)
    except Community.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = CommunitySerializer(community)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = CommunitySerializer(community, partial=True, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        community.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)