from django.db import models

class Location(models.Model):
    name    = models.CharField(max_length=60, unique=True)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    class Meta:
        ordering = ['name']


class University(models.Model):
    name        = models.CharField(max_length=60, unique=True)
    email_ext   = models.CharField(max_length=60, unique=True)
    created     = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated     = models.DateTimeField(auto_now_add=False, auto_now=True)

    class Meta:
        ordering = ['name']


class Community(models.Model):
    name        = models.CharField(max_length=60)
    university  = models.ForeignKey(University, on_delete=models.CASCADE)
    created     = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated     = models.DateTimeField(auto_now_add=False, auto_now=True)

    class Meta:
        ordering = ['university', 'name']
        unique_together = ('name', 'university')
